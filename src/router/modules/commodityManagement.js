import Layout from '@/layout/Index'
const commodityManagementRoute = {
    path: '/commodityManagement',
    component: Layout,
    redirect: '/commodityManagement/manageGoods',
    name: 'commodityManagement',
    meta:{title: '商品管理'},
    children: [
      {
        path: 'commodityRule',
        name: 'commodityRule',
        component: () => import('@/views/commodityManagement/CommodityRule.vue'),
        meta:{title: '商品规格'},
      },
      {
        path: 'manageGoods',
        name: 'manageGoods',
        component: () => import('@/views/commodityManagement/ManageGoods.vue'),
        meta:{title: '管理商品'},
      },
      {
        path: 'goodsAdd',
        name: 'goodsAdd',
        component: () => import('@/views/commodityManagement/components/GoodsAdd.vue'),
        meta:{title: '商品增加'},
      },
      {
        path: 'goodsUpdate/:commodity/',
        name: 'goodsUpdate',
        component: () => import('@/views/commodityManagement/components/GoodsUpdate.vue'),
        meta:{title: '商品修改'},
      },
    ]
}
export default commodityManagementRoute