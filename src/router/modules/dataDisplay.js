import Layout from '@/layout/Index'

const dataDisplayRouter = {
    path: '/dataDisplay',
    component: Layout,
    redirect: '/dataDisplay/index',
    children: [
      {
        path: 'index',
        name: 'dataDisplay',
        component: () => import('@/views/dataVisualization/DataDisplay.vue'),
        meta:{title: '首页',affix:true},
      },
    ]
}
export default dataDisplayRouter