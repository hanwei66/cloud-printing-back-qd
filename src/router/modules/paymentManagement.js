import Layout from '@/layout/Index'

const paymentManagementRoute = {
    path: '/paymentManagement',
    component: Layout,
    redirect: '/paymentManagement/wechatPaymentConfiguration',
    name: 'paymentManagement',
    meta:{title: '支付管理'},
    children: [
      {
        path: 'wechatPaymentConfiguration',
        name: 'wechatPaymentConfiguration',
        component: () => import('@/views/paymentManagement/WechatPaymentConfiguration.vue'),
        meta:{title: '微信支付配置'},
      },
    ]
}
export default paymentManagementRoute