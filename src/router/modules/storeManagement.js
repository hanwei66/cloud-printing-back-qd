import Layout from '@/layout/Index'
const storeManagementRoute = {
    path: '/storeManagement',
    component: Layout,
    redirect: '/storeManagement/storeConfiguration',
    name: 'storeManagement',
    meta:{title: '门店管理'},
    children: [
      // {
      //   path: 'storeConfiguration',
      //   name: 'storeConfiguration',
      //   component: () => import('@/views/StoreManagement/StoreConfiguration.vue'),
      //   meta:{title: '门店配置'},
      // },
      {
        path: 'storeList',
        name: 'storeList',
        component: () => import('@/views/StoreManagement/StoreList.vue'),
        meta:{title: '门店列表'},
      },
      {
        path: 'clerkManagement',
        name: 'clerkManagement',
        component: () => import('@/views/StoreManagement/ClerkManagement.vue'),
        meta:{title: '店员管理'},
      },
    ]
}

export default storeManagementRoute