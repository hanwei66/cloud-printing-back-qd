import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
//打印文件
import Print from 'vue-print-nb'
Vue.use(Print)
import axios from 'axios'
Vue.prototype.$axios = axios
Vue.use(ElementUI)
Vue.config.productionTip = false
import {formatter} from '@/utils/timeUtil'
Vue.filter('dateYMDHMSFormat',formatter)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
