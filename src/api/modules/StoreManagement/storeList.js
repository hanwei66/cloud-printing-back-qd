import request from '@/utils/axiosRequest.js'

//获取所有门店
export function getAllStore() {
  return request({
    url: '/store/getAllStore',
    method: 'get',
  })
}
//获取用户拥有或员工属于的门店
export function getUserHaveOrBelongToStore() {
  return request({
    url: '/store/getUserHaveOrBelongToStore',
    method: 'get',
  })
}
//新增门店
export function addStore(data) {
  return request({
    url: '/store/addStore',
    method: 'post',
    data:data
  })
}
//修改门店
export function updateStore(data) {
  return request({
    url: '/store/updateStore',
    method: 'post',
    data:data
  })
}
//删除门店
export function deleteStoreById(data) {
  return request({
    url: '/store/deleteStoreById',
    method: 'post',
    data:data
  })
}
//删除多个门店
export function deleteStoreByIdList(data) {
  return request({
    url: '/store/deleteStoreByIdList',
    method: 'post',
    data:data
  })
}
