import request from '@/utils/axiosRequest.js'
//获取所有订单
export function getPagingStore(data) {
    return request({
      url: 'order/getPagingStore',
      method: 'get',
      params:data
    })
}
//根据订单id获取订单
export function getStoreByOrderId(data) {
  return request({
    url: 'order/getStoreByOrderId',
    method: 'get',
    params:data
  })
}
//根据订单id修改订单状态
export function updateOrderStautsByOrderId(data) {
  return request({
    url: 'order/updateOrderStautsByOrderId',
    method: 'POST',
    data:data
  })
}
//修改订单备注
export function updateOrderRemarksByOrderId(data) {
  return request({
    url: 'order/updateOrderRemarksByOrderId',
    method: 'get',
    params:data
  })
}
